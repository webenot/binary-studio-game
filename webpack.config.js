const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const Cleanlugin = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const HTMLPlugin = require('html-webpack-plugin');
require("@babel/polyfill");

const isProduction = (process.env.NODE_ENV === 'production');

module.exports = {
    entry: {
        'app' : ["@babel/polyfill",
            path.resolve(__dirname, 'src') + '/index.js',
            path.resolve(__dirname, 'src') + '/sass/style.sass',
        ],
    },
    devServer: {
        inline: true,
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
    },
    context: path.resolve(__dirname, 'src'),
    devtool: (isProduction) ? false : 'inline-source-map',
    output: {
        filename: 'boundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    mode: (isProduction) ? 'production' : 'development',
    watch: !isProduction,
    module: {
        rules: [
            // js
            {
                test: /\.m?js$/i,
                exclude: /^(node_modules)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        configFile: "./babel.config.js",
                        cacheDirectory: true
                    }
                }
            },

            // HTML
            {
                test: /\.pug$/i,
                use: [
                    {
                        loader: 'pug-loader',
                        options: {
                            sourceMap: !isProduction,
                            pretty: true,
                        }
                    }
                ]
            },
            // sass
            {
                test: /\.sass$/i,
                resolve: {
                    extensions: ['.scss', '.sass'],
                },
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: !isProduction,
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: !isProduction,
                            },
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: !isProduction,
                            },
                        },
                    ],
                    fallback: 'style-loader',
                }),
            },
            // Images
            {
                test: /\.(png|gif|jpe?g)$/i,
                loaders: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                        },
                    },
                    'img-loader',
                ],

            },
            // Fonts
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                        },
                    },
                ],
            },
            // SVG
            {
                test: /\.svg$/i,
                use: {
                    loader: 'svg-url-loader',
                    options: {
                        encoding: 'base64'
                    }
                }
            },
        ],

    },
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(process.env.NODE_ENV)
        }),
        new HTMLPlugin({
            filename: 'index.html',
            template: './jade/index.pug',
            mobile: true
        }),
        new CopyPlugin(
            [
                { from: './img', to: 'img', },
            ],
            {
                ignore: [
                    { glob: 'svg/*', },
                ],
            },
        ),
        new ExtractTextPlugin('css/[name].css'),
        new Cleanlugin()
    ],
};

// PRODUCTION ONLY
if(isProduction) {
    module.exports.plugins.push(
        new ImageminPlugin({
            test: /\.(png|jpe?g|gif|svg)$/i,
            jpegtran: { progressive: true }
        }),
    );
    module.exports.plugins.push(
        new webpack.LoaderOptionsPlugin({
            minimize: true,
        }),
    );
}