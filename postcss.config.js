const autoprefixer = require('autoprefixer');

module.exports = {
    plugins: [
        autoprefixer({
            browsers:['ie >= 6', 'last 100 version']
        })
    ],
};